<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'order';
    
    public function toString(){
        return 'number: '.$this->orderNumber;
    }
    
    public function customerObj(){
        return $this->belongsTo('App\User','customer');
    }
    
}
