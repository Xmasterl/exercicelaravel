<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleTranslation extends Model
{
    //
    protected $table = 'role_translations';
}
