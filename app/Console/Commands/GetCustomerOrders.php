<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\OrderController;

class GetCustomerOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:getCustomerOrders {idCustomer}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $controller;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->controller=new OrderController();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $orders= $this->controller->getAllCustomer($this->argument('idCustomer'));
        
        echo json_encode($orders);
//        foreach ($orders as $order) {
////            echo $order->toString();
//            var_dump(json_encode($order));
//        }
    }
}
