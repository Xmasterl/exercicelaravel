<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Http\Controllers\OrderController;

class GetAllOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:getAll';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Select all orders for admin';
    
    private $controller;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->controller=new OrderController();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        
        $orders= $this->controller->getAllAdmin();
        

//        $orders = Order::all();
        echo json_encode($orders);
//        foreach ($orders as $order) {
//            echo $order->customerObj->name;
//            var_dump($order->customerObj->name);
//        }
    }
}
