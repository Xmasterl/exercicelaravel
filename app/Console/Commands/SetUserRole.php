<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\UserController;
use App\User;

class SetUserRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:setRole {idUser} {idRole}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $controller;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->controller=new UserController();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        
        $user=$this->controller->setRole($this->argument('idUser'), $this->argument('idRole'));
        
        echo json_encode($user);
    }
}
