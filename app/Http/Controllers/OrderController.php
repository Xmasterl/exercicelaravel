<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    //
    
    public function getAllAdmin(){
        $orders=Order::join('user','user.id','=','order.customer')
                ->join('role','role.id','=','user.role')
                ->join('role_translations',function($join){
                    $join->on('role_translations.language','=','user.language')
                            ->on('role_translations.idRole','=','user.role');
                })
                ->select('order.orderNumber','user.name','user.email','role_translations.translation as role','order.total','order.status')
                ->get();
        
        return $orders;
    }
    
    public function getAllCustomer($idCustomer){
        $orders=Order::join('user','user.id','=','order.customer')
                ->select('order.orderNumber','user.name','user.email','order.total','order.status')->where('customer',$idCustomer)
                ->get();
        
        return $orders;
    }
}
