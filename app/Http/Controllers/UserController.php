<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    //
    public function setRole($idUser, $idRole){
        $user=User::find($idUser);
        $user->role=$idRole;
        $user->save();
        
        return $user;
    }
}
